# Read more about app structure at http://docs.appgyver.com

module.exports =

  # See styling options for tabs and other native components in app/common/native-styles/ios.css or app/common/native-styles/android.css
  # tabs: [
  #   {
  #     title: "Index"
  #     id: "index"
  #     location: "example#getting-started" # Supersonic module#view type navigation
  #   }
  #   {
  #     title: "Settings"
  #     id: "settings"
  #     location: "example#settings"
  #   }
  #   {
  #     title: "Internet"
  #     id: "internet"
  #     location: "http://google.com" # URLs are supported!
  #   }
  # ]

  rootView:
    location: "profile#index"

  preloads: [
    {
      id: "login"
      location: "login#index"
    }
    {
      id: "profile"
      location: "profile#index"
    }
    {
      id: "session"
      location: "session#index"
    }
    {
      id: "search"
      location: "search#index"
    }
  ]
  #call the drawer from here in android
  # drawers:
    # right:
    #   id: "search"
    #   location: "search#index"
    #   showOnAppLoad: false
    # options:
    #   animation: "slide"

  #
  # initialView:
  #   id: "initialView"
  #   location: "example#initial-view"
