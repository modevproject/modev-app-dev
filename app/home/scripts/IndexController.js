angular
  .module('home')
  .controller('IndexController', function($scope, supersonic) {
  	$scope.hello = "hello world";
  	$scope.alertme = function(){
  		alert("alerted");
  	};
  	$scope.logme = function(){
  		
        supersonic.device.platform().then( function(platform) {
          alert(
            "Name: " + platform.name + "\n" +
            "Version: " + platform.version + "\n" +
            "Model: " + platform.model
            );
        });
  	};
  	$scope.jquerytest = function(){
  		$('.first-title').css("color", "red");
  	}
  	$scope.tweentest = function(){
  		TweenMax.to($('.box1'), 2, {left:200});
  	}
  	$scope.tweentimelinetest = function(){
  		var tl = new TimelineMax();
  		tl.to($('.box2'), 3, {left:200}).to($('.box3'), 1, {left:300});
  	}
    // Controller functionality here
  });
