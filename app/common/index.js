angular.module('common', [
  // Declare here all AngularJS dependencies that are shared by all modules.
  'supersonic','ionic','ngResource'
])
.service('$LocalStorage', function() {
    this.save = function(key,item){
      if(this.isNull(key)){
      localStorage.setItem(key, JSON.stringify(item));
      return true
      } else{
        return false
      }
    }
    this.get = function(key){
      if(!this.isNull(key)){
        var value = localStorage.getItem(key);
        var item = JSON.parse(value);
        return item
      } else{
        return false
      }
    }
    this.update = function(key,value){
      if(!this.isNull(key)){
       localStorage.setItem(key, JSON.stringify(item)); 
     }else{
      return false
     }
    }
    this.delete = function(key){
      if(!this.isNull(key)){
        localStorage.removeItem(key);
        return true 
      }else{
        return false
      }
    }
    this.isNull = function(key){
      return (localStorage.getItem(key) === null)
    }
})
.factory('User', function($resource) {
    return $resource('http://modev.appteam.io/api/v1/users')
})
.factory('SignIn', function($resource) {
    return $resource('http://modev.appteam.io/api/v1/users/sign_in')
})
.factory('Speakers', function($resource) {
    return $resource('http://modev.appteam.io/api/v1/speakers')
})
.directive('ngHexButton', function() {
  return {
    restrict: 'E',
    scope:{
     icon: '@',
     size: '@',
     fontSize: '@',
     badge: '@',
     color: '@'
    },
    templateUrl: "/templates/hex-button.html",
    link: function(scope,element,attrs){
      
    }
  }
})
.directive('ngHexImage', function() {
  return {
    restrict: 'E',
    scope:{
       image: '=src',
     size: '@',
     border: '@',
     borderColor: '@'
    },
    templateUrl: "/templates/hex-image.html",
    link: function(scope,element,attrs){
      
    }
  }
})
.directive('ngHeaderMenu', function($timeout) {
  return {
    restrict: 'E',
    scope:{
     menus: '=',
     color: '@',
     bg: '@',
     borderColor: '@'
    },
    templateUrl: "/templates/header-menu.html",
    link: function(scope,element,attrs){
     scope.selected = function(menu){
      angular.forEach(scope.menus, function(value,key){
        value.isActive = false;
      });
      $timeout(function(){
        menu.isActive = true;
      },100)
     }
    }
  }
})
.directive('ngHexList', function($timeout) {
  return {
    restrict: 'E',
    scope:{
      people: '=',
      title: '@',
      color: '@'
    },
    templateUrl: "/templates/hex-list.html",
    link: function(scope,element,attrs){

    }
  }
})
.directive('ngTimeSlot', function($ionicPopover) {
  return {
    restrict: 'E',
    scope:{
      color: '@',
      time: '@',
      duration: '@',
      title: '@',
      description: '@',
      location: '@',
      icon:'@',
      iconSize:'@',
      iconAction:'&'
    },
    templateUrl: "/templates/time-slot.html",
    link: function(scope,element,attrs){
      scope.popover;
      $ionicPopover.fromTemplateUrl('templates/popover.html', {
        scope: scope,
      }).then(function(popover){
        scope.popover = popover;
      });
      scope.showActions = function($event){
         scope.popover.show($event);
      }
    }
  }
})
.directive('ngTab', function() {
  return {
    restrict: 'A',
    scope:{
      icon: '@',
      isActive: '@'
    },
    template: "<i class='fa fa-{{icon}}'></i>",
    link: function(scope,element,attrs){
      element.addClass("button");
      element.css({
       fontSize:"25px",
       margin:"2px",
       borderRadious:"10px",
       border: "none", 
       padding: "8px 5px",
      });
      if(scope.isActive == "true"){
        element.css({
         color: "#FFF",
         background: "#f88432"
        });
      }

    }
  }
})
.directive('ngDateCard', function() {
  return {
    restrict: 'E',
    scope:{
      month:'@',
      day:'@',
      isActive:'@',
      bg:'@',
      borderColor:'@'
    },
    template: "{{month}}<br/>{{day}}",
    link: function(scope,element,attrs){
      element.addClass("date-card")
      scope.$watch('isActive', function(newValue, oldValue) {
        if(scope.isActive == "true"){
          element.addClass("active")
          element.css({
            background: scope.bg,
            borderColor: scope.borderColor
          });
        }else{
           element.removeClass("active")
          element.css({
            background: 'gray',
          });
        }
      });
    }
  }
})
.directive('ngSwiper', function($timeout,supersonic) {
  return {
    restrict: 'A',
    transclude: true,
    scope:{
      ready: '@',
      slide:'='

    },
    templateUrl: "/templates/swiper.html",
    link: function(scope,element,attrs){
      element.css({
        width: '100%',
        fontSize: '29px'
      });

      var setup = false;
      scope.mySwiper;
      scope.$watch('ready', function(newValue, oldValue) {
        if(newValue == "true" && !setup){
          $timeout(function(){
          scope.mySwiper = new Swiper (element, {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            initialSlide: scope.slide.id,
            onTransitionStart: function(swiper){
              $timeout(function(){
                scope.slide.id = swiper.activeIndex;
              },0);
            }
          }) 

          scope.mySwiper.slideTo(scope.slide.id);
          setup = true;
          scope.$watch('slide.id', function(newValue, oldValue) {
            scope.mySwiper.slideTo(newValue);
          });
            },100);
        }
 
      });


    }
  }
})
.directive('ngSwiperSlide', function() {
  return {
    restrict: 'A',
    transclude: true,
    scope:{
    },
    template: "<ng-transclude></ng-transclude>",
    link: function(scope,element,attrs){
      // alert();
      element.addClass('swiper-slide');
      element.css({
        width:'100%',
      });     
    }
  }
})
.factory('DataGetter', function() {

    return {
      "speakers": [
        {
          "image": "/images/speaker_badger01.jpg",
          "name": "Mark Badger",
          "title": "CapTech Consulting",
          "company": "Creative Director"
        },
        {
          "image": "/images/speaker_bell01.jpg",
          "name": "Terena Bell",
          "title": "In Every Language",
          "company": "CEO"
        },
        {
          "image": "/images/speaker_chandler01.jpg",
          "name": "Carolyn Chandler",
          "title": "Author, 'A Project Guide to UX Design' and",
          "company": "'Adventures in Experience Design''"
        },

        {
          "image": "/images/speaker_cranfordteague01.jpg",
          "name": "Jason CranfordTeague",
          "title": "Capital One",
          "company": "Sr. Creative Director, Digital Design'"
        },
        {
          "image": "/images/speaker_cueva01.jpg",
          "name": "Amy Cueva",
          "title": "Mad*Pow",
          "company": "Founder / CXO"
        },
        {
          "image": "/images/speaker_ferguson01.jpg",
          "name": "Laura Ferguson",
          "title": "Create.io",
          "company": "CTO & Co-Founder"
        },
        {
          "image": "/images/speaker_gurses01.jpg",
          "name": "Levent Gurses",
          "title": "Movel",
          "company": "President"
        },
        {
          "image": "/images/speaker_haag01.jpg",
          "name": "Margaret Haag",
          "title": "Capital One",
          "company": "Senior UI Designer"
        },
        {
          "image": "/images/speaker_hay01.jpg",
          "name": "Stephanie Hay",
          "title": "Capital One",
          "company": "Director of Content Strategy"
        },
        {
          "image": "/images/speaker_helms01.jpg",
          "name": "David Helms",
          "title": "Radius Networks",
          "company": "Chief Product Officer"
        },
        {
          "image": "/images/speaker_hoober01.jpg",
          "name": "Steven Hoober",
          "title": " 'Author ,DesigningMobileInterfaces'",
          "company": ""
        },
        {
          "image": "/images/speaker_huang01.jpg",
          "name": "Evelyn Huang",
          "title": "Head of Design Thinking & Strategy",
          "company": "Capital One"
        },
        {
          "image": "/images/speaker_kan01.jpg",
          "name": "Boris Kan",
          "title": "Chief of Design",
          "company": "Create.io"
        },
        {
          "image": "/images/speaker_kesler01.jpg",
          "name": "Matt Kessler",
          "title": "Engineering Lead, Digital Solutions",
          "company": "CapTech Consulting"
        },
        {
          "image": "/images/speaker_kobar01.jpg",
          "name": "Chris Kobar",
          "title": "Sr. Human Factors Engineer",
          "company": "SRA International"
        },
        {
          "image": "/images/speaker_lanier01.jpg",
          "name": "Wren Lanier",
          "title": "Designer",
          "company": "Highrise"
        },
        {
          "image": "/images/speaker_leyzorovich01.jpg",
          "name": "Maximn Leyzorovich",
          "title": "Senior Experience Designer",
          "company": "nclud"
        },
        {
          "image": "/images/speaker_lovin01.jpg",
          "name": "Brian Lovin",
          "title": "Product Designer",
          "company": "Buffer"
        },
        {
          "image": "/images/speaker_mace01.jpg",
          "name": "Michael Mace",
          "title": "VP of Mobile",
          "company": "User Testing"
        },
        {
          "image": "/images/speaker_majeed01.jpg",
          "name": "Sabrina Majeed",
          "title": "Product Design Manager",
          "company": "Buzzfeed"
        },
        {
          "image": "/images/speaker_montgomery01.jpg",
          "name": "Gordon Montgomery",
          "title": "Gordon Montgomery Coaching",
          "company": "Principal"
        },
        {
          "image": "/images/speaker_newman01.jpg",
          "name": "Dan Newman",
          "title": "Deputy Creative Director",
          "company": "NPR"
        },
        {
          "image": "/images/speaker_romanobergstrom01.jpg",
          "name": "Jennifer Romano Bergstrom",
          "title": "UX Researcher",
          "company": "Facebook"
        },
        {
          "image": "/images/speaker_snook01.jpg",
          "name": "Jason Snook",
          "title": "CX Director",
          "company": "CapTech Consulting"
        },
        {
          "image": "/images/speaker_strohl01.jpg",
          "name": "Jonathan Strohl",
          "title": "UX Researcher",
          "company": "Fors Marsh Group"
        },
        {
          "image": "/images/speaker_wendel01.jpg",
          "name": "Steve Wendel",
          "title": "Principal Scientist",
          "company": "HelloWallet"
        },
        {
          "image": "/images/speaker_whalen01.jpg",
          "name": "John Whalen",
          "title": "CEO/Founder",
          "company": "Brilliant Experience"
        },
        {
          "image": "/images/speaker_winters01.jpg",
          "name": "Mick Winters",
          "title": "Capital One",
          "company": "Head of Digital Design, Bank"
        }
      ]
    }
});