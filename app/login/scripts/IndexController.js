angular
  .module('login')
  .controller('IndexController', function($scope, $timeout,$LocalStorage, supersonic, User,SignIn) {
    $scope.user;
    $scope.errors = null;
    //remove error if user is entry new credentials
    $scope.$watch('user',function(newVal,oldVal){$scope.errors = null;},true)
    $scope.loginStates = {
      'home' : true,
      'email' : false,
      'signUp' : false
    };
    $scope.emailLogin = function(){
      $scope.errors = null;
      if($scope.loginStates.email){
        //if we are on the email state process theuser and password
        $scope.signInUser();
      }
      else if($scope.loginStates.signUp){
        $scope.saveUser();
      }
      else{
        $scope.loginStates.home = false;
        $scope.loginStates.signUp = false;
        $timeout(function(){
          $scope.loginStates.email = true;
        },500);
      }
    }
    $scope.signUpLogin = function(){
      $scope.errors = null;
        $scope.loginStates.home = false;
        $scope.loginStates.email = false;
        $timeout(function(){
          $scope.loginStates.signUp = true;
        },500);
    }
    $scope.backHome = function(){
      $scope.errors = null;
      $scope.loginStates.signUp = false;
      $scope.loginStates.email = false;
      $timeout(function(){
      $scope.loginStates.home = true;
      },100);
    }

    $scope.signInUser = function(){
      $scope.signIn = new SignIn();
      $scope.signIn.user_login = {"email" : $scope.user.email,"password" : $scope.user.password};
      $scope.signIn.$save(function(u,response){
          $LocalStorage.save('user',u);
          $scope.errors = null;
          $scope.userReady();
          $scope.user = null;
        },function(error){
          $scope.errors = 'wrong crendentials'
        });
    }
    $scope.saveUser = function(){
      if($scope.user.password == $scope.user.password_confirmation){
        $scope.newuser = new User();
        $scope.newuser.user = {"email" : $scope.user.email,"password" : $scope.user.password};
        $scope.newuser.$save(function(u,response){
            $LocalStorage.save('user',u);
            $scope.errors = null;
            $scope.userReady();
            $scope.user = null;
          },function(error){
              angular.forEach(error.data,function(value,key){
                if($scope.errors == null)
                  $scope.errors = key+" "+value;
              });
            });
      }else{
        $scope.errors = 'Password and confirmation do not match'
      }
    }

    $scope.userReady = function(){
      if($LocalStorage.get('user')){
        supersonic.data.channel('logged').publish('logged in');
        supersonic.ui.modal.hide({animate: true});
      }
    }











    // $scope.response;
    // $scope.hideBt = false;
    // $scope.inputAnime = false;

  });
