angular.module('login', [
  // Declare any module-specific AngularJS dependencies here
  'common','ngAnimate'
])
.service('AnimationService', function() {
    this.animeDisplay = new TimelineMax();
})
.animation('.hide-me', function(AnimationService) {
  return {
    addClass: function(element, className, done) {
      AnimationService.animeDisplay.to(element, 0.2, {opacity:0, ease: Power4.easeOut});
     },
    removeClass: function(element, className, done) {
      AnimationService.animeDisplay.to(element, 0.2, {opacity:1});
     }
  };
})
.animation('.input-home', function(AnimationService) {
  return {
    addClass: function(element, className, done) {
    AnimationService.animeDisplay.to(element, 0.3, {opacity:0, height:0, ease: Power4.easeOut});
     },
  };
})
.animation('.input-email', function(AnimationService) {
  return {
    addClass: function(element, className, done) {
    AnimationService.animeDisplay.to(element, 0.5, {opacity:1, height:100, ease: Power4.easeOut});
     },
  };
})
.animation('.input-signup', function(AnimationService) {
  return {
    addClass: function(element, className, done) {
    AnimationService.animeDisplay.to(element, 0.5, {opacity:1, height:150, ease: Power4.easeOut});
     },
  };
});

