angular
  .module('session')

.controller('IndexController', function($scope,$timeout,Speakers, DataGetter, supersonic, $ionicPopover,$ionicScrollDelegate) {
 
    $scope.image = '/images/speaker_badger01.jpg';
    $scope.focused; //used to determine when the page has been viewed for the first time may differ from ios to android. also usuful if you want to give the time for the dom to load
    $scope.people = DataGetter.speakers;
    $scope.speakers = Speakers.query({token: 'qrcVuszDAAcyMsgbfxsp'})
    $scope.myActiveSlide = 1;
    $scope.slide = { id: 0 }; //required to be an object to be able to work on two way binding with directives
    $scope.menus = [
      {name : "Sessions", isActive : false},
      {name : "Calendar", isActive : true},
      {name : "Speakers", isActive : false}
    ];
    $scope.testActive = false;
    // Use supersonic features inside ->
    supersonic.device.ready.then( function() {
      //Diffrent behaviour on android: since is view visibility is trigger a first time on app load even if its not visible.
      //Fixed by ignoring the first visibility on android
      var stopListening = supersonic.ui.views.current.whenVisible( function() {
        if(angular.isUndefined($scope.focused)|| $scope.focused == null){
          //ios here for the first time page in focus
          supersonic.logger.log("First time viewed on ios not yet on android");     
          $scope.focused = true;   
        }else if ($scope.focused == false){
          $scope.focused = true; 
          //android here for the first time page in focus
          supersonic.logger.log("First time viewed on android");     
            
            //remove header on android devices
            supersonic.ui.navigationBar.hide({animated: false} ).then( function() {
              supersonic.logger.debug("Navigation bar hidden without animation.");
            });

        }else{
          supersonic.logger.log("page in focus");     
        }

      });
    });

    // Bottom tabs switching:

    $scope.showProfile = function(){
      supersonic.ui.views.find("profile").then(function(preloadedView){
        supersonic.ui.layers.replace(preloadedView);
      });
    }
  });
