angular
  .module('profile')

.controller('IndexController', function($scope,$timeout,$LocalStorage,DataGetter,Speakers,supersonic,$ionicScrollDelegate) {
    $scope.menus = [
      {name : "My Profile", isActive : false},
      {name : "People I Met", isActive : true},
      {name : "My Sessions", isActive : false}
    ];
    $scope.speakers = Speakers.query({token: 'qrcVuszDAAcyMsgbfxsp'})
    $scope.people = DataGetter.speakers;
    $scope.image = "/system/speakers/profile_images/000/000/001/original/376135_10150629969073747_370082780_n.jpg?1424445925";
    $scope.user = $LocalStorage.get('user');
    $scope.visible;
    // Use supersonic features inside ->
    supersonic.device.ready.then( function() {
        var unsubscribe = supersonic.data.channel('logged').subscribe( function(message, reply) {
         $scope.visible = 'well, what happened?';
         $scope.$apply();
        });

      $scope.$watch('user', function(newValue, oldValue) {
        if(newValue)
          console.log();
        else
          $scope.showlogin();
      });
        // uncomment to show login
        supersonic.ui.views.find("search").then( function(rightDrawer) {
            //call the drawer from here in IOS. use structure.coffee for android and comment this one out
            supersonic.ui.drawers.init(rightDrawer,{side: "right",width: 200});
            supersonic.ui.drawers.updateOptions({shadow: false, animation: {type: "slide"} });
        });
        //listen to when page visible to remove header. acts diffrently on ios and android
        var stopListening = supersonic.ui.views.current.whenVisible( function() {
          //remove header on android devices
          // $scope.user = $LocalStorage.get('user');
       
          supersonic.ui.navigationBar.hide({animated: false} );
          stopListening();
        });
    });

    $scope.showlogin = function(){
      supersonic.ui.views.find("login").then(function(modalView){
        supersonic.ui.modal.show(modalView, {animate: true});
      });
    }
    $scope.showSession = function(){
      supersonic.ui.views.find("session").then(function(preloadedView){
        supersonic.ui.layers.replace(preloadedView);
      });
    }
    $scope.openDrawer = function(){
       supersonic.ui.drawers.open("right");
    }
    $scope.test;
    $scope.status;
    $scope.findUser = function(){
     $scope.user = $LocalStorage.get('user');
    };
    $scope.logOut = function(){
      $LocalStorage.delete('user');
      $scope.user = null;
    }
   
  });
